package com.example.booksapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button allBooksButton, currentlyReadingBooksButton, alreadyReadingBooksButton, favoriteBooksButton, aboutButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();

        allBooksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AllBooksActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initViews() {
        allBooksButton = findViewById(R.id.allBooksButton);
        currentlyReadingBooksButton = findViewById(R.id.currentlyReadingBooksButton);
        alreadyReadingBooksButton = findViewById(R.id.alreadyReadingBooksButton);
        favoriteBooksButton = findViewById(R.id.favoriteBooksButton);
        aboutButton = findViewById(R.id.aboutButton);
    }
}