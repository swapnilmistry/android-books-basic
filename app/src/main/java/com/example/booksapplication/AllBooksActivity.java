package com.example.booksapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class AllBooksActivity extends AppCompatActivity {

    private RecyclerView booksRecyclerView;
    private BooksRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_books);
        adapter = new BooksRecyclerViewAdapter(this);
        booksRecyclerView = findViewById(R.id.booksRecyclerView);
        booksRecyclerView.setAdapter(adapter);
        booksRecyclerView.setLayoutManager(new GridLayoutManager(this, 2));

        ArrayList<BooksModel> books = new ArrayList<>();
        books.add(new BooksModel(1, "Test Book Name", 10, "https://media.sproutsocial.com/uploads/2017/02/10x-featured-social-media-image-size.png", "SD", "LD" ));
        adapter.setBooks(books);
    }
}